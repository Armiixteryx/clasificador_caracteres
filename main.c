/*
 * 1: Contar vocales.
 * 2: Contar letras.
 * 3: Contar dígitos.
 * 4: Contar palabras.
*/

#include <stdio.h> //Básico.
#include <string.h> //Para strlen().
#include <ctype.h> //Para tolower().

void flush()
{
    int c;
    while((c = getchar()) != '\n' && c != EOF);
}

int main() {
    char ch[300], salida='n';
    int chP=0, i=0, vocales=0, letras=0, palabras=1, digitos=0;
    
    while(tolower(salida) != 's')
    {
        //Limpiar variables:
        vocales=0; letras=0; palabras=1; digitos=0;
        
        //Solicitud de datos:
        printf("Introduce lo que quieras:\n");
        gets(ch);
        printf("\n");
        
        //Comprobar si se ha introducido algo:
        if(strlen(ch) == 0)
        {
            printf("No has introducido nada.\n");
        }
        else
        {
            for(i=0; i<strlen(ch); i++)
            {
                //Convertir mayúsculas en minúsculas:
                ch[i] = tolower(ch[i]);
                
                //Obtener valor ASCII de las letras:
                chP = ch[i];
                
                //Contar vocales:
                switch(ch[i])
                {
                    case 'a': vocales++; break;
                    case 'e': vocales++; break;
                    case 'i': vocales++; break;
                    case 'o': vocales++; break;
                    case 'u': vocales++; break;
                }
                
                //Contar letras:
                if(chP >= 97 && chP <= 122)
                {
                    letras++;
                }
                
                //Contar dígitos:
                if(chP >= 48 && chP <= 57)
                {
                    digitos++;
                }
                
                //Contar palabras:
                if(chP == 32)
                {
                    palabras++;
                }
            }
            //Impresión de resultados:
            printf("Hay %d vocales.\n", vocales);
            printf("Hay %d letras.\n", letras);
            printf("Hay %d dígitos.\n", digitos);
            printf("Has introducido %d palabras.\n\n", palabras);
        }
        
        //Comprobación de salida:
        printf("¿Deseas salir? s/n\nTu opción: ");
        salida = getchar();
        printf("\n");
        flush();
    }
        return 0;
}
